//
//  DatabaseManager.swift
//  FutureMind
//
//  Created by Mateusz Frycie on 19/01/2019.
//  Copyright © 2019 Mateusz Frycie. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class DatabaseManager {
    
    static let sharedInstance = DatabaseManager()
    
    var managedContext : NSManagedObjectContext?
    
    private init() { }
    
    func createItem(id: Int16, title: String?, desc: String?, modificationDate: Date?, redirectUrl: String?, imageUrl: String?) {
        
        guard let context = managedContext else {
            
            print("Error while loading context")
            return
        }
        
        let item = Item(context: context)
        item.orderId = id
        item.title = title
        item.desc = desc
        item.modificationDate = modificationDate
        item.redirectUrl = redirectUrl
        item.imageUrl = imageUrl
        
        saveContext()
    }
    
    func readAllItems() -> [Item]? {
        
        guard let context = managedContext else {
            
            print("Error while loading context")
            return nil
        }
        
        let request : NSFetchRequest<Item> = Item.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(Item.orderId), ascending: true)
        request.sortDescriptors = [sort]
        
        do {
            return try context.fetch(request)
        } catch {
            print("Error while reading from context: \(error.localizedDescription)")
        }
        
        return nil
    }
    
    func deleteAllItems() {
        
        guard let context = managedContext else {
            
            print("Error while loading context")
            return
        }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Item")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do
        {
            try context.execute(deleteRequest)
            try context.save()
        }
        catch
        {
            print ("Error while deleting all from context: \(error.localizedDescription)")
        }
    }
    
    func saveContext() {
        
        guard let context = managedContext else {
            
            print("Error while loading context")
            return
        }
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                print("Error while saving context: \(error.localizedDescription)")
            }
        }
    }
}

