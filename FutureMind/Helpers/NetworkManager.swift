//
//  NetworkManager.swift
//  FutureMind
//
//  Created by Mateusz Frycie on 19/01/2019.
//  Copyright © 2019 Mateusz Frycie. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SwiftyJSON

class NetworkManager {
    
    static let sharedInstance = NetworkManager()

    let baseURL: String = "https://www.futuremind.com/recruitment-task"
    
    private init() { }
    
    func getTaskData (completion: @escaping (String) -> Void) {
        
        Alamofire.request(baseURL).responseJSON {
        response in
            if response.result.isSuccess {
                print("Success: \(String(describing: response))")
                
                let taskJSON :JSON = JSON(response.result.value!)
                
                taskJSON.array?.forEach { item in
                    
                    self.parseTaskData(json: item)
                }
                
                completion("success")
                
            } else {
                let errorMsg = "Error while getting TaskData: \(String(describing: response.result.error?.localizedDescription)))"
                
                print(errorMsg)
                completion(errorMsg)
            }
        }
    }
    
    func parseTaskData(json: JSON) {
        
        let orderId: Int16 = json["orderId"].int16Value
        
        let title = json["title"].stringValue
        
        let desc = self.getDescriptionNoRedirectUrlFormatted(desc: json["description"].stringValue)
      
        let modificationDate = self.getDateStringToDateFormatted(date: json["modificationDate"].stringValue)
        
        let redirectUrl = self.getReirectUrlFromDescription(desc: json["description"].stringValue)
        
        let imageUrl = json["image_url"].stringValue
        
        let databaseManager = DatabaseManager.sharedInstance
            databaseManager.createItem(id: orderId, title: title, desc: desc, modificationDate: modificationDate, redirectUrl: redirectUrl, imageUrl: imageUrl)
    }

    
    // MARK: - Helpers
    
    func getDateStringToDateFormatted(date: String?) -> Date? {
        
        if date != nil {
            
            let formatter = DateFormatter()
            
            formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            formatter.dateFormat = "yyyy-MM-dd"
            
            return formatter.date(from: date!)!
            
        } else {
         
            print("Error while converting date from string")
            return nil
        }
    }
    
    func getReirectUrlFromDescription(desc: String?) -> String? {
        
        if let index = (desc?.range(of: "http://")?.lowerBound) {
            
            return String(desc!.suffix(from: index))
        } else if let index = (desc?.range(of: "https://")?.lowerBound) {
            
            return String(desc!.suffix(from: index))
        } else {
            
            print("No redirectUrl in description")
            return nil
        }
    }
    
    func getDescriptionNoRedirectUrlFormatted(desc: String?) -> String? {
        
        if let index = (desc?.range(of: "\thttp://")?.lowerBound) {
            
           return String(desc!.prefix(upTo: index))
        } else if let index = (desc?.range(of: "\thttps://")?.lowerBound) {
            
            return String(desc!.prefix(upTo: index))
        } else {
            
            print("No redirectUrl in description")
            return desc ?? nil
        }
    }
}
