//
//  ItemTableViewCell.swift
//  FutureMind
//
//  Created by Mateusz Frycie on 20/01/2019.
//  Copyright © 2019 Mateusz Frycie. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "ItemCell"
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var imageContainer: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
