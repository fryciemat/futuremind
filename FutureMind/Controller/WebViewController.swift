//
//  WebViewController.swift
//  FutureMind
//
//  Created by Mateusz Frycie on 19/01/2019.
//  Copyright © 2019 Mateusz Frycie. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    var selectedItem: Item!
    
    @IBOutlet var webView: WKWebView!
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (selectedItem.redirectUrl) != nil {
            
            let url = URL(string: selectedItem.redirectUrl!)!
            webView.load(URLRequest(url: url))
            
            let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
            toolbarItems = [refresh]
            navigationController?.isToolbarHidden = false
            
        } else {
            
            self.showAlert()
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helpers
    
    func showAlert() {
        
        let alert = UIAlertController(title: "Error", message: "No valid URL", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Delegate methods
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }
}
