//
//  ListViewController.swift
//  FutureMind
//
//  Created by Mateusz Frycie on 19/01/2019.
//  Copyright © 2019 Mateusz Frycie. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var itemsArray:[Item]? = []
    
    @IBOutlet var itemTableView: UITableView!
    @IBAction func refreshPressed(_ sender: UIBarButtonItem) {
        
            self.loadTableViewData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemTableView.delegate = self
        itemTableView.dataSource = self
        
        let databaseManager = DatabaseManager.sharedInstance
        itemsArray = databaseManager.readAllItems()
        
        configureTableView()
        loadTableViewData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? WebViewController,
            let indexPath = itemTableView.indexPathForSelectedRow {
            destination.selectedItem = itemsArray?[indexPath.row]
        }
    }
    
    // MARK:- Helpers
    
    func configureTableView() {
        
        itemTableView.rowHeight = UITableView.automaticDimension
        itemTableView.estimatedRowHeight = 600.0
    }
    
    func loadTableViewData() {
        
        SVProgressHUD.show()
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        let networkManager = NetworkManager.sharedInstance
        networkManager.getTaskData { (completion) -> () in
            if completion == "success" {
                
                self.itemsArray = (DatabaseManager.sharedInstance.readAllItems())
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.itemTableView.reloadData()
                }
            } else {
                SVProgressHUD.dismiss()
                self.showAlert(msg: completion)
            }
            
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            
        }
    }
    
    func showAlert(msg: String) {
        
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getDateToStringFormatted(date: Date?) -> String? {
        
        let formatter = DateFormatter()
        
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        formatter.dateFormat = "yyyy-MM-dd"
        
        if let formattedDate = date {
            
            return formatter.string(from: (formattedDate))
        } else {
            
            return nil
        }
    }
    
    //MARK: - Delegate methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as? ItemTableViewCell else {
            fatalError("Unexpected Index Path")
        }
        
        let item = itemsArray?[indexPath.row]
        
        cell.titleLabel.text = item?.title
        cell.descriptionLabel.text = item?.desc
        cell.dateLabel.text = self.getDateToStringFormatted(date: item?.modificationDate)
        
        if let stringUrl = item?.imageUrl {
            let url = URL(string:(stringUrl))!
            cell.imageContainer.af_setImage(withURL: url)
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
}

